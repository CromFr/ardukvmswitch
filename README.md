
# Install

## Arduino program

1. Edit the defines atop of `ardukvm.ino` to match the pins of your arduino nano (or other)
2. Build and flash `ardukvm.ino`

## Process-serial

This is the program that runs on the PC to read the outputs from the arduino and issue the control commands.


1. Install `ddcutil` (for controlling screen output sources)
2. Edit the config lines atop of `ardukvm-process-serial`
3. Run `ardukvm-process-serial` (you may need to run as root, since [ddcutil requires access to i2c files](https://blog.tcharles.fr/ddc-ci-screen-control-on-linux/))