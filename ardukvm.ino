#define PIN_SWITCH_1 7
#define PIN_SWITCH_2 6
#define PIN_SWITCH_3 5
#define PIN_KEYBOARD_MOUSE 4
#define PIN_SCREEN1 3
#define PIN_SCREEN2 2

#define PIN_LED_SWITCH_1 13
#define PIN_LED_SWITCH_2 12
#define PIN_LED_SWITCH_3 11
#define PIN_LED_KEYBOARD_MOUSE 10
#define PIN_LED_SCREEN1 9
#define PIN_LED_SCREEN2 8

#define DELAY_DEBOUNCE 100

bool switchConfig[3][3] = {{true, true, true},{true, true, true},{true, true, true}};
int activatedSwitchLed = PIN_LED_SWITCH_1;

void sendSwitchCommand(int target, bool devsToSwitch[3]){
	char cmd[32] = {0};
	sprintf(cmd, "%d %d %d\n",
		devsToSwitch[0]? target : -1,
		devsToSwitch[1]? target : -1,
		devsToSwitch[2]? target : -1);
	Serial.write(cmd);
}

int getLedMatchingPin(int pin){
	switch(pin){
		case PIN_SWITCH_1: return PIN_LED_SWITCH_1;
		case PIN_SWITCH_2: return PIN_LED_SWITCH_2;
		case PIN_SWITCH_3: return PIN_LED_SWITCH_3;
		case PIN_KEYBOARD_MOUSE: return PIN_LED_KEYBOARD_MOUSE;
		case PIN_SCREEN1: return PIN_LED_SCREEN1;
		case PIN_SCREEN2: return PIN_LED_SCREEN2;
	}
	Serial.write("Error\n");
	return -1;
}

void switchLedsOff(){
	digitalWrite(PIN_LED_SWITCH_1, false);
	digitalWrite(PIN_LED_SWITCH_2, false);
	digitalWrite(PIN_LED_SWITCH_3, false);
	digitalWrite(PIN_LED_KEYBOARD_MOUSE, false);
	digitalWrite(PIN_LED_SCREEN1, false);
	digitalWrite(PIN_LED_SCREEN2, false);
}

int pinToOffset(int pin){
	switch(pin){
		case PIN_SWITCH_1: return 0;
		case PIN_SWITCH_2: return 1;
		case PIN_SWITCH_3: return 2;
		case PIN_KEYBOARD_MOUSE: return 0;
		case PIN_SCREEN1: return 1;
		case PIN_SCREEN2: return 2;
	}
	Serial.write("Error\n");
	return -1;
}

void setup() 
{
	Serial.begin(115200);

	pinMode(PIN_SWITCH_1, INPUT_PULLUP);
	pinMode(PIN_SWITCH_2, INPUT_PULLUP);
	pinMode(PIN_SWITCH_3, INPUT_PULLUP);
	pinMode(PIN_KEYBOARD_MOUSE, INPUT_PULLUP);
	pinMode(PIN_SCREEN1, INPUT_PULLUP);
	pinMode(PIN_SCREEN2, INPUT_PULLUP);

	pinMode(PIN_LED_SWITCH_1, OUTPUT);
	pinMode(PIN_LED_SWITCH_2, OUTPUT);
	pinMode(PIN_LED_SWITCH_3, OUTPUT);
	pinMode(PIN_LED_KEYBOARD_MOUSE, OUTPUT);
	pinMode(PIN_LED_SCREEN1, OUTPUT);
	pinMode(PIN_LED_SCREEN2, OUTPUT);
	switchLedsOff();
}

void handleSwitchPress(int pressedPin){
	int switchIndex;
	switch(pressedPin){
		case PIN_SWITCH_1: switchIndex = 0; break;
		case PIN_SWITCH_2: switchIndex = 1; break;
		case PIN_SWITCH_3: switchIndex = 2; break;
	}
	int ledPin = getLedMatchingPin(pressedPin);
	digitalWrite(PIN_LED_SWITCH_1, ledPin == PIN_LED_SWITCH_1);
	digitalWrite(PIN_LED_SWITCH_2, ledPin == PIN_LED_SWITCH_2);
	digitalWrite(PIN_LED_SWITCH_3, ledPin == PIN_LED_SWITCH_3);
	digitalWrite(PIN_LED_KEYBOARD_MOUSE, switchConfig[switchIndex][0]);
	digitalWrite(PIN_LED_SCREEN1, switchConfig[switchIndex][1]);
	digitalWrite(PIN_LED_SCREEN2, switchConfig[switchIndex][2]);

	bool devsToSwitch[3] = {false, false, false};
	bool displayCustom = false;

	while(!digitalRead(pressedPin)){
		if(!digitalRead(PIN_KEYBOARD_MOUSE)){
			displayCustom = true;
			devsToSwitch[0] = true;
		}
		if(!digitalRead(PIN_SCREEN1)){
			displayCustom = true;
			devsToSwitch[1] = true;
		}
		if(!digitalRead(PIN_SCREEN2)){
			displayCustom = true;
			devsToSwitch[2] = true;
		}

		if(displayCustom){
			digitalWrite(PIN_LED_KEYBOARD_MOUSE, devsToSwitch[0]);
			digitalWrite(PIN_LED_SCREEN1, devsToSwitch[1]);
			digitalWrite(PIN_LED_SCREEN2, devsToSwitch[2]);
		}
	}

	if(devsToSwitch[0] || devsToSwitch[1] || devsToSwitch[2]){
		// Save custom switch
		for(int i=0; i<3; i++){
		    switchConfig[switchIndex][i] = devsToSwitch[i];
		}
	}

	activatedSwitchLed = ledPin;
	sendSwitchCommand(switchIndex, switchConfig[switchIndex]);
	switchLedsOff();

	delay(DELAY_DEBOUNCE);
}

void handleDevicePress(int pressedPin){
	bool devsToSwitch[3] = {false, false, false};
	int counter = 0;
	
	digitalWrite(PIN_LED_KEYBOARD_MOUSE, false);
	digitalWrite(PIN_LED_SCREEN1, false);
	digitalWrite(PIN_LED_SCREEN2, false);

	// Handle pressedPin as pressed
	devsToSwitch[pinToOffset(pressedPin)] = true;
	digitalWrite(getLedMatchingPin(pressedPin), true);

	delay(DELAY_DEBOUNCE);

	// start loop on key release
	while(!digitalRead(pressedPin)){}

	while(digitalRead(PIN_SWITCH_1)
	&& digitalRead(PIN_SWITCH_2)
	&& digitalRead(PIN_SWITCH_3)){
		if(!digitalRead(pressedPin)){
			// cancel switch
			switchLedsOff();
			delay(DELAY_DEBOUNCE);
			while(!digitalRead(pressedPin)){}
			return;
		}

		if(!digitalRead(PIN_KEYBOARD_MOUSE)){
			devsToSwitch[0] = true;
			digitalWrite(PIN_LED_KEYBOARD_MOUSE, true);
		}
		if(!digitalRead(PIN_SCREEN1)){
			devsToSwitch[1] = true;
			digitalWrite(PIN_LED_SCREEN1, true);
		}
		if(!digitalRead(PIN_SCREEN2)){
			devsToSwitch[2] = true;
			digitalWrite(PIN_LED_SCREEN2, true);
		}

		digitalWrite(PIN_LED_SWITCH_1, counter / (10000 / 3) == 0);
		digitalWrite(PIN_LED_SWITCH_2, counter / (10000 / 3) == 1);
		digitalWrite(PIN_LED_SWITCH_3, counter / (10000 / 3) == 2);

		counter = (counter + 1) % 10000;
	}

	int target;
	if(!digitalRead(PIN_SWITCH_1)) target = 0;
	else if(!digitalRead(PIN_SWITCH_2)) target = 1;
	else if(!digitalRead(PIN_SWITCH_3)) target = 2;

	activatedSwitchLed = -1;
	sendSwitchCommand(target, devsToSwitch);
	switchLedsOff();

	while(!digitalRead(PIN_SWITCH_1)
	|| !digitalRead(PIN_SWITCH_2)
	|| !digitalRead(PIN_SWITCH_3)){}

	delay(DELAY_DEBOUNCE);
}

void loop()
{
	if(activatedSwitchLed >= 0)
		digitalWrite(activatedSwitchLed, true);

	if(!digitalRead(PIN_SWITCH_1))
		handleSwitchPress(PIN_SWITCH_1);
	if(!digitalRead(PIN_SWITCH_2))
		handleSwitchPress(PIN_SWITCH_2);
	if(!digitalRead(PIN_SWITCH_3))
		handleSwitchPress(PIN_SWITCH_3);

	if(!digitalRead(PIN_KEYBOARD_MOUSE))
		handleDevicePress(PIN_KEYBOARD_MOUSE);
	if(!digitalRead(PIN_SCREEN1))
		handleDevicePress(PIN_SCREEN1);
	if(!digitalRead(PIN_SCREEN2))
		handleDevicePress(PIN_SCREEN2);
}